import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { WorkInfo } from '@/types/WorkInfo'
// const workInfo = ref<WorkInfo[]>([])
export const useWorkInfo = defineStore('workInfo', () => {
  const worktime = ref<WorkInfo[]>([
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
    {
        id: 4,
    },
    {
        id: 5,
    }
])

  return { worktime }
})
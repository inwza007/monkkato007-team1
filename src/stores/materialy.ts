import { ref } from 'vue'

type material = {
    id:number
    name: string
    price: number
    unit: string
    quantitymin: number
    quantity: number
  }
  export const materialitem : material[] =[
    {
        id: 1,
        name: 'coffee beans',
        price: 300,
        unit: 'kg',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 2,
        name: 'green tea powder',
        price: 250,
        unit: 'kg',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 3,
        name: 'Thai tea powder',
        price: 240,
        unit: 'kg',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 4,
        name: 'Milk',
        price: 100,
        unit: 'liter',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 5,
        name: 'Water',
        price: 50,
        unit: 'liter',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 6,
        name: 'suger',
        price: 50,
        unit: 'kg',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 7,
        name: 'Cocoa powder',
        price: 260,
        unit: 'kg',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 8,
        name: 'Syrup',
        price: 80,
        unit: 'liter',
        quantitymin:5,
        quantity: 0
    },
    {
        id: 9,
        name: 'Sweetened condensed milk',
        price: 120,
        unit: 'liter',
        quantitymin:5,
        quantity: 0
    }
]

export { materialitem as  material }

import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/other/member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    {id:1,name:'มิก กี้',tel:'0812345678',points: 100},
    {id:2,name:'ตูน ตู้น',tel:'0912345678',points: 100},
    {id:3,name:'อาร์ม อ้าร์ม',tel:'0659822958',points: 100}
])
const currentMember = ref<Member|null>()
const searchMembers = (tel:string) =>{
    const index = members.value.findIndex((item) => item.tel ===  tel)
    if (index < 0) {
        currentMember.value = null
    }
    currentMember.value = members.value[index]
} 
function clear (){
  currentMember.value = null
}
  return { members,searchMembers,clear,currentMember }
})
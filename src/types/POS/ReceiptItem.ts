import type { Product } from "./Product"

type ReceiptItem = {
    id:string,
    name:string,
    price:number,
    unit:number,
    productId:number,
    product?:Product
  }

export{type ReceiptItem}
import type { ReceiptMaterialItem } from './ReceiptMaterialItem'
type ReceiptMaterial = {
  id: number
  createdDate?: Date
  totalBefore: number;
  createdDateString: string
  total: number
  change: number
  paymentType: string
  receiptStockItem: ReceiptMaterialItem[]
}

export type { ReceiptMaterial }

type BuyMaterial = {
    id: number;
    name: string;
    price: number;
    unit: string;
    quantity: number;
}

export { type BuyMaterial }

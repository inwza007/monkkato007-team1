
type ReceiptWorkTime = {
    id:string,
    name:string,
    price:number,
    unit:number,
    productId:number,
  }

export{type ReceiptWorkTime}